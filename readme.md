# Waste4Change Web Intern Challange

This is a task for an intern program challange at W4C. Was assigned as a prerequisite for joining their internship program.

## Task

- Create an API doc with postmant
- Create the API
- Design and code the UI according to the mockup given
- Integrate the backend and the frontend

## Rules

The rules of this project are to use both Laravel and MySql. 

## Tech

- Laravel
- MySql
- Bootstrap (yes bootstrap, with some modification and custom css)

## Task Result

1. API Documentation
Postman Doc URL : -
2. Gitlab
Gitlab URL : https://gitlab.com/cah-kangkung/w4c-challange
3. Website
Web URL : https://cahkangkung.000webhostapp.com/

## How To

I'm using Laravel Migration for the database. So the first  thing you need to do is to create the database with the appropriate name, and then just run

```php artisan migrate```

## Problem

When i was going to make the Documentation. I was petrified...

I was so focus on the deadline i didn't realized that i need to actually make REST API with Laravel and integarte it with
Vue or any other frontend framework, not to make the standard Laravel MVC with templating engine like Blade to present the view. 
But since i don't have enough time to re-architec the whole thing, i'll stick with it for now.

